import copy
import numpy as np
import os, pickle
import time
import torch
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader, Dataset
from sklearn.metrics import average_precision_score
from argparse import ArgumentParser

from transformer import Encoder, Decoder, EncoderDecoder, MultiHeadedAttention, PositionwiseFeedForward, \
    PositionalEncoding, Embeddings, EncoderLayer, DecoderLayer, Generator, subsequent_mask, EncoderPredictor, Predictor
from utils import SimpleLossCompute, LabelSmoothing, NoamOpt


def make_model(src_vocab, tgt_vocab, N=6,
               d_model=512, d_ff=2048, h=8, dropout=0.1):
    "Helper: Construct a model from hyperparameters."
    c = copy.deepcopy
    attn = MultiHeadedAttention(h, d_model)
    ff = PositionwiseFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)
    model = EncoderDecoder(
        Encoder(EncoderLayer(d_model, c(attn), c(ff), dropout), N),
        Decoder(DecoderLayer(d_model, c(attn), c(attn),
                             c(ff), dropout), N),
        nn.Sequential(Embeddings(d_model, src_vocab), c(position)),
        nn.Sequential(Embeddings(d_model, tgt_vocab), c(position)),
        Generator(d_model, tgt_vocab))

    # This was important from their code.
    # Initialize parameters with Glorot / fan_avg.
    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform(p)
    return model

def make_predictor_model(src_vocab, d_first, d_out, N=6,
               d_model=512, d_ff=2048, h=8, dropout=0.1, d_predictor=[], pos_weight=None):
    "Helper: Construct a model from hyperparameters."
    c = copy.deepcopy
    attn = MultiHeadedAttention(h, d_model)
    ff = PositionwiseFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)

    model = EncoderPredictor(
        Encoder(EncoderLayer(d_model, c(attn), c(ff), dropout), N),
        Predictor(d_first, d_out, d_model, h, d_predictor),
        nn.Sequential(Embeddings(d_model, src_vocab), c(position)), pos_weight)
    #import pdb; pdb.set_trace()
    # This was important from their code.
    # Initialize parameters with Glorot / fan_avg.
    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform(p)
    return model

def run_epoch(data_iter, model, loss_compute):
    "Standard Training and Logging Function"
    start = time.time()
    total_tokens = 0
    total_loss = 0
    tokens = 0
    for i, batch in enumerate(data_iter):
        out = model.forward(batch.src, batch.trg,
                            batch.src_mask, batch.trg_mask)
        loss = loss_compute(out, batch.trg_y, batch.ntokens)
        total_loss += loss
        total_tokens += batch.ntokens
        tokens += batch.ntokens
        if i % 50 == 1:
            elapsed = time.time() - start
            print("Epoch Step: %d Loss: %f Tokens per Sec: %f" %
                    (i, loss / batch.ntokens, tokens / elapsed))
            start = time.time()
            tokens = 0
    return total_loss / total_tokens


class Batch:
    "Object for holding a batch of data with mask during training."

    def __init__(self, src, trg=None, pad=0):
        self.src = src
        self.src_mask = (src != pad).unsqueeze(-2)
        if trg is not None:
            self.trg = trg[:, :-1]
            self.trg_y = trg[:, 1:]
            self.trg_mask = \
                self.make_std_mask(self.trg, pad)
            self.ntokens = (self.trg_y != pad).data.sum()

    @staticmethod
    def make_std_mask(tgt, pad):
        "Create a mask to hide padding and future words."
        tgt_mask = (tgt != pad).unsqueeze(-2)
        tgt_mask = tgt_mask & Variable(
            subsequent_mask(tgt.size(-1)).type_as(tgt_mask.data))
        return tgt_mask


def data_gen(V, batch, nbatches):
    "Generate random data for a src-tgt copy task."
    for i in range(nbatches):
        data = torch.from_numpy(np.random.randint(1, V, size=(batch, 10)))
        data[:, 0] = 1
        src = Variable(data, requires_grad=False)
        tgt = Variable(data, requires_grad=False)
        yield Batch(src, tgt, 0)

def get_mask(seqs):
    mask_cutoffs = np.argmin(seqs.sum(axis=2), axis=1)
    mask = np.ones((seqs.shape[0], seqs.shape[1], 1))
    for i, cutoff in enumerate(mask_cutoffs):
        if cutoff != 0:
            mask[i][cutoff:] = 0
    return mask

def protein_gen(pickle_fname, seqs_fname, results_dir, batch_size):
    with open(pickle_fname, 'rb') as f:
        annotations = pickle.load(f)
    seqs = np.load(seqs_fname)
    seqs_train, seqs_test, labels_train, labels_test, goterms = filter_sequences(annotations, seqs)

    with open(os.path.join(results_dir, "_model_functions.p"), 'wb') as f:
        pickle.dump(goterms, f)

    train_mask = get_mask(seqs_train)
    test_mask = get_mask(seqs_test)
    seqs_train = np.argmax(seqs_train, axis=2)
    seqs_test = np.argmax(seqs_test, axis=2)
    num_samples = 10
    train_dataset = ProteinDataset(seqs_train, train_mask, labels_train)
    test_dataset = ProteinDataset(seqs_test, test_mask, labels_test)
    train_dataloader = DataLoader(train_dataset, batch_size)
    test_dataloader = DataLoader(test_dataset, batch_size)
    pos_weight = (labels_train.shape[0]-labels_train.sum(axis=0))/labels_train.sum(axis=0)

    num_cat = len(goterms)
    max_len = train_dataset.data.shape[1]

    return train_dataloader, test_dataloader, num_cat, max_len, pos_weight


def filter_sequences(Annot, Seqs):
    goterms = np.asarray(Annot['GO_IDs'])
    prots = np.asarray(Annot['prot_IDs'])
    Y_train = np.concatenate((Annot['train_annots'], Annot['valid_annots']))
    Y_test = Annot['test_annots']
    train_idx = np.asarray(np.concatenate((Annot['train_prot_inds'], Annot['valid_prot_inds'])))
    test_idx = np.asarray(Annot['test_prot_inds'])
    # only specifc function
    test_funcs = np.where(np.logical_and(Y_train.sum(axis=0) > 50, Y_train.sum(axis=0) <= 500))[1]
    Y_train = Y_train[:, test_funcs]
    Y_test = Y_test[:, test_funcs]
    goterms = goterms[test_funcs]

    # prots with at least 2 GO terms
    train_idx_min_go = np.where(Y_train.sum(axis=1) >= 2)[0]
    test_idx_min_go = np.where(Y_test.sum(axis=1) >= 2)[0]
    Y_train = Y_train[train_idx_min_go]
    Y_test = Y_test[test_idx_min_go]

    train_idx = train_idx[train_idx_min_go]
    test_idx = test_idx[test_idx_min_go]

    train_chain_ids = prots[train_idx]
    test_chain_ids = prots[test_idx]

    seqs_train = Seqs[train_idx]
    seqs_test = Seqs[test_idx]

    # remove zero-column GO terms
    test_funcs = np.where(Y_test.sum(axis=0) > 0)[1]
    print ("### Number of functions: ", len(test_funcs))
    Y_train = Y_train[:, test_funcs]
    Y_test = Y_test[:, test_funcs]
    goterms = goterms[test_funcs]

    return seqs_train, seqs_test, Y_train, Y_test, goterms


class ProteinDataset(Dataset):
    def __init__(self, data, mask, labels):
        self.data = torch.from_numpy(data.astype(np.float)).long()
        self.labels = torch.from_numpy(labels.astype(np.float)).long()
        self.mask = torch.from_numpy(mask.astype(np.float)).long()

    def __getitem__(self, index):
        protein = self.data[index]
        mask = self.mask[index]
        label = self.labels[index]
        return protein, mask, label

    def __len__(self):
        return self.data.shape[0]

def train(batch_size=64, num_epochs=100, N=6, d_model=512, d_ff=2048, h=8, dropout=0.1, gpu=False, debug=True, model_save=False, validation=False):
    pickle_fname = 'annotation_int8'
    seqs_fname = 'sequences_int.npy'
    print('Loading data', flush=True)
    train_gen, test_gen, num_cat, max_len, pos_weight = protein_gen(pickle_fname, seqs_fname, './', batch_size)
    print(pos_weight, flush=True)
    #criterion = LabelSmoothing(size=tgt_vocab, padding_idx=0, smoothing=0.0)
    #model = make_model(src_vocab, tgt_vocab, N, d_model, d_ff, h, dropout)
    d_predictor = [64]
    num_residues = 22
    model = make_predictor_model(num_residues, d_model, num_cat, d_model=d_model, h=1, N=N,
                                 d_predictor=d_predictor, pos_weight=torch.from_numpy(pos_weight))
    if gpu: model = model.cuda()
    model_opt = NoamOpt(model.src_embed[0].d_model, 1, 400,
                        torch.optim.Adam(model.parameters(), lr=1e-3, betas=(0.9, 0.98), eps=1e-9))
    print('Training', flush=True)
    for epoch in range(num_epochs):
        print('Epoch number: {}'.format(epoch), flush=True)
        model.train()
        model_opt.optimizer.zero_grad()
        #train_data_generator = data_gen(src_vocab, 30, 20)
        #loss_compute = SimpleLossCompute(model.generator, criterion, model_opt)
        #run_epoch(train_data_generator, model, loss_compute)
        i = 0
        for train_data, train_mask, train_labels in train_gen:
            if gpu:
                train_data = train_data.cuda()
                train_mask = train_mask.cuda()
                train_labels = train_labels.cuda()

            predictions = model.forward(train_data, train_mask)
            loss = model.loss(predictions.double(), train_labels.double())
            loss = loss.mean()
            loss.backward()
            model_opt.step()
            if debug:
                print('Training loss: {}'.format(loss.data), flush=True)
                print(torch.sigmoid(predictions)[:5], flush=True)
            #i += batch_size; break
            #print(i, flush=True)
            #import pdb; pdb.set_trace()
        if validation:
            model.eval()
            test_preds = []
            test_losses = []
            test_labs = []
            i = 0
            for test_data, test_mask, test_labels in test_gen:
                test_labs.append(test_labels)
                if gpu:
                    test_data = test_data.cuda()
                    test_mask = test_mask.cuda()
                    test_labels = test_labels.cuda()
    
                predictions = model.forward(test_data, test_mask)
                test_preds.append(predictions.cpu().data)
                loss = model.loss(predictions.double(), test_labels.double()).mean(dim=1)
                test_losses.append(loss.cpu().data)
            test_loss = torch.cat(test_losses).mean().numpy()
            test_labs = torch.cat(test_labs).numpy()
            test_preds = torch.sigmoid(torch.cat(test_preds)).numpy()
            print(test_preds[:5], flush=True)
            aupr_macro = average_precision_score(test_labs, test_preds, average='macro')
            aupr_micro = average_precision_score(test_labs, test_preds, average='micro')
            print('Validation loss: {}'.format(test_loss), flush=True)
            print('Validation AUPR macro: {}'.format(aupr_macro), flush=True)
            print('Validation AUPR micro: {}'.format(aupr_micro), flush=True)
            print('----------', flush=True)
        if debug is False and model_save is True:
            torch.save(model.state_dict(), 'model_wl__N1_dff512_dm32')
        #run_epoch(val_data_generator, model, loss_compute)

parser = ArgumentParser()
parser.add_argument('--debug', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    train(4, N=1, d_ff=32, h=1, d_model=32, gpu=True, debug=args.debug)
